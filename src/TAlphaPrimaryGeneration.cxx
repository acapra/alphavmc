// Adapted from:
//------------------------------------------------
// The Virtual Monte Carlo examples
// Copyright (C) 2007, 2008 Ivana Hrivnacova
// All rights reserved.
//
// For the licensing terms see geant4_vmc/LICENSE.
// Contact: vmc@pcroot.cern.ch
//-------------------------------------------------

/// Implementation of the  class 
/// Geant4 ExampleN06 adapted to Virtual Monte Carlo \n
/// Id: ExN06PrimaryGeneratorAction.cc,v 1.3 2004/04/02 11:54:29 maire Exp \n 
/// GEANT4 tag Name: geant4-07-00-cand-01 
///
/// \date 16/05/2005
/// \author I. Hrivnacova; IPN, Orsay

#include <utility>
#include <functional>

#include <TVirtualMC.h>
#include <TVirtualMCStack.h>
#include <TGenPhaseSpace.h>
#include <TPDGCode.h>
#include <TDatabasePDG.h>
#include <TParticlePDG.h>
#include <TVector3.h>
#include <TMath.h>

#include "TAlphaEvent.h"
#include "TAlphaPrimaryGenerator.h"

#define MPC 0.139566 // LGCP : This is the mass of a charged pion
#define MP0 0.134976 // LGCP : This is the mass of a neutral pion

#define NFS 14       // LGCP : This is the number of branches/decay modes of p-pbar annihilation 

// LGCP : Below are the number of particles involved when a certain branch/decay mode is taken
Int_t NumPar[NFS] = 
{ 
     2,                                 // p0 p0   
     4,                                 // p0 p0 p0 p0 
     3,                                 // p+ p- p0
     4,                                 // p+ p- p0 p0
     5,                                 // p+ p- p0 p0 p0
     6,                                 // p+ p- p0 p0 p0 p0      
     4,                                 // p+ p- p+ p-
     5,                                 // p0 p+ p- p+ p-
     6,                                 // p0 p0 p+ p- p+ p-
     7,                                 // p0 p0 p0 p+ p- p+ p-
     6,                                 // p+ p- p+ p- p+ p-
     7,                                 // p+ p- p+ p- p+ p- p0
     2,                                 // p+ p-
     3                                  // p0 p0 p0      
};                              


// LGCP : Below are the masses of the particles involved when a certain branch/annihilation mode is taken
Double_t ParMas[NFS][7] = 
{ 
     { MP0, MP0,   0,   0,   0,   0,   0},   // p0 p0
     { MP0, MP0, MP0, MP0,   0,   0,   0},   // p0 p0 p0 p0
     { MP0, MPC, MPC,   0,   0,   0,   0},   // p0 p+ p-
     { MP0, MP0, MPC, MPC,   0,   0,   0},   // p0 p0 p+ p-
     { MP0, MP0, MPC, MPC, MP0,   0,   0},   // p0 p0 p0 p+ p-
     { MP0, MP0, MPC, MPC, MP0, MP0,   0},   // p0 p0 p0 p0  p+ p-
     { MPC, MPC, MPC, MPC,   0,   0,   0},   // p+ p- p+ p-
     { MP0, MPC, MPC, MPC, MPC,   0,   0},   // p0 p+ p- p+ p-
     { MP0, MP0, MPC, MPC, MPC, MPC,   0},   // p0 p0 p+ p- p+ p-
     { MP0, MP0, MPC, MPC, MPC, MPC, MP0},   // p0 p0 p0 p+ p- p+ p-
     { MPC, MPC, MPC, MPC, MPC, MPC,   0},   // p+ p- p+ p- p+ p-
     { MPC, MPC, MPC, MPC, MPC, MPC, MP0},   // p+ p- p+ p- p+ p- p0
     { MPC, MPC,   0,   0,   0,   0,   0},   // p+ p-
     { MP0, MP0, MP0,   0,   0,   0,   0}    // p0 p0 p0    
};


// LGCP : Below are the GEANT3 particle id codes for the particles involved when a certain branch/annihilation mode is taken

#define PI0 111     // PDG particle codes
#define PIP 211
#define PIM -211
#define MUP -13
#define MUM 13

Int_t ParNum[NFS][7] = 
{    
     {   PI0,   PI0,   0,     0,     0,     0,     0},   // p0 p0        
     {   PI0,   PI0,   PI0,   PI0,   0,     0,     0},   // p0 p0 p0 p0                
     {   PI0,   PIP,   PIM,   0,     0,     0,     0},   // p0 p+ p-
     {   PI0,   PI0,   PIP,   PIM,   0,     0,     0},   // p0 p0 p+ p-
     {   PI0,   PI0,   PIP,   PIM,   PI0,   0,     0},   // p0 p0 p0 p+ p-   
     {   PI0,   PI0,   PIP,   PIM,   PI0,   PI0,   0},   // p0 p0 p0 p0 p+ p-            
     {   PIP,   PIM,   PIP,   PIM,   0,     0,     0},   // p+ p- p+ p-
     {   PI0,   PIP,   PIM,   PIP,   PIM,   0,     0},   // p0 p+ p- p+ p-
     {   PI0,   PI0,   PIP,   PIM,   PIP,   PIM,   0},   // p0 p0 p+ p- p+ p-
     {   PI0,   PI0,   PIP,   PIM,   PIP,   PIM,   PI0}, // p0 p0 p0 p+ p- p+ p-
     {   PIP,   PIM,   PIP,   PIM,   PIP,   PIM,   0},   // p+ p- p+ p- p+ p-
     {   PIP,   PIM,   PIP,   PIM,   PIP,   PIM,   PI0}, // p+ p- p+ p- p+ p- p0
     {   PIP,   PIM,   0,     0,     0,     0,     0},   // p+ p-
     {   PI0,   PI0,   PI0,   0,     0,     0,     0}    // p0 p0 p0 
};

// LGCP : This MaxWeight function still puzzles me.
// LGCP : This is supposed to be, according to Pablo Genova, a correction to the phase space...
Double_t MaxWeigth[NFS] = 
{
     1.000000, // p0 p0  
     0.118145, // p0 p0 p0 p0  
     0.411809, // p0 p+ p-  
     0.118144, // p0 p0 p+ p-
     0.027187, // p0 p0 p0 p+ p-
     0.005407, // p0 p0 p0 p0 p+ p-
     0.118187, // p+ p- p+ p-  
     0.027182, // p0 p+ p- p+ p-  
     0.005405, // p0 p0 p+ p- p+ p-
     0.000963, // p0 p0 p0 p+ p- p+ p- 
     0.005405, // p+ p- p+ p- p+ p-  
     0.000967, // p+ p- p+ p- p+ p- p0  
     1.000000, // p+ p-  
     0.411814  // p0 p0 p0 
};

// LGCP : These are the branching ratios for the different p-pbar annihilation modes
Double_t BraRat[NFS] = 
{  
     .00028,   // p0 p0  
     .03000,   // p0 p0 p0 p0               
     .06900,   // p0 p+ p-  
     .09300,   // p0 p0 p+ p-
     .23300,   // p0 p0 p0 p+ p-
     .02800,   // p0 p0 p0 p0 p+ p- 
     .06900,   // p+ p- p+ p-  
     .19600,   // p0 p+ p- p+ p-  
     .16600,   // p0 p0 p+ p- p+ p-  
     .04200,   // p0 p0 p0 p+ p- p+ p-  
     .02100,   // p+ p- p+ p- p+ p-  
     .01900,   // p+ p- p+ p- p+ p- p0  
     .00320,   // p+ p-
     .00760    // p0 p0 p0   
}; 

Double_t BraRatCum[NFS];

#include <TTree.h>
extern TTree* gFRDtree;
extern Float_t gFRDx,gFRDy,gFRDz;
extern int gEntry;


ClassImp(TAlphaPrimaryGenerator)

//_____________________________________________________________________________
TAlphaPrimaryGenerator::TAlphaPrimaryGenerator(TVirtualMCStack* stack) 
  : TObject(),
    fStack(stack),
    fPdg(kPositron),
    fKinEnergy(500.e+01),
    fDirX(1.),
    fDirY(0.),
    fDirZ(0.),
    fPolAngle(0.),
    fNofPrimaries(1)
{
/// Standard constructor
/// \param stack  The VMC stack
}

//_____________________________________________________________________________
TAlphaPrimaryGenerator::TAlphaPrimaryGenerator()
  : TObject(),
    fStack(0),
    fPdg(0),
    fKinEnergy(0.),
    fDirX(0.),
    fDirY(0.),
    fDirZ(0.),
    fPolAngle(0.),
    fNofPrimaries(0)
{    
/// Default constructor
}

//_____________________________________________________________________________
TAlphaPrimaryGenerator::~TAlphaPrimaryGenerator() 
{
/// Destructor  
}

//
// private methods
//

#include <Riostream.h>
#include <TF1.h>
//_____________________________________________________________________________
std::pair <Int_t,Double_t> TAlphaPrimaryGenerator::GeneratePrimary()
{    
/// Add one primary particle to the user stack 
/// (derived from TVirtualMCStack).
  // Track ID (filled by stack)
  Int_t ntr;
 
  // Option: to be tracked
  Int_t toBeDone = 1; 
  
  // Position
  Double_t vx  = 0.; 
  Double_t vy  = 0.; 
  Double_t vz =  0.;
  Double_t tof = 0.;

  if( 0 ) // cosmic
    {
    gEvent->SetCosmic( kTRUE );
    
    if(0){ // Richard's
       vx = 5.*(gRandom->Rndm()-0.5);
       vy = 14.;
       vz = 20.*(gRandom->Rndm()-0.5);

      TVector3 MCVertex( vx, vy, vz );
      gEvent->SetMCVertex( MCVertex );
      //gEvent->SetCosmic( kTRUE );

      Double_t Cte = TMath::Sqrt(gRandom->Rndm());
      Double_t Ste = TMath::Sqrt(1-Cte*Cte);
      Double_t phi = gRandom->Rndm() * 2*TMath::Pi();
      Double_t Cph = TMath::Cos(phi);
      Double_t Sph = TMath::Sin(phi);

      Float_t PMu = 2.;
      Float_t p[3] ={PMu*(Float_t)(Ste*Cph), -PMu*(Float_t)Cte, PMu*(Float_t)(Ste*Sph)};
      
      Int_t mu = (gRandom->Rndm() > 0.5)?  MUP:MUM; // 5=mu+  6=mu-

      fStack->PushTrack(toBeDone, -1, mu, p[0], p[1], p[2], PMu, vx, vy, vz, tof, 
			0, 0, 0, 
			kPPrimary, ntr, 1., 0);
      }
      
      if(1)
      {
	TF1* fcos = new TF1("fcos","TMath::Power(TMath::Cos(x),2.)",-TMath::PiOver2(),TMath::PiOver2());
	Double_t phi=fcos->GetRandom();
	phi = TMath::PiOver2() - phi;
	Double_t csi = gRandom->Uniform(0.,TMath::TwoPi());
	
	Double_t ux = TMath::Sin(csi)*TMath::Cos(phi);
	Double_t uy = TMath::Sin(phi);
	Double_t uz = TMath::Cos(csi)*TMath::Cos(phi);
	
	Double_t x0 = gRandom->Uniform(-10.,10.);
	Double_t z0 = gRandom->Uniform(-26.,26.);
		
	vy = 19.;
	vx = x0 + ux * vy/uy;
	vz = z0 + uz * vy/uy;

	TVector3 MCVertex( vx, vy, vz );
        gEvent->SetMCVertex( MCVertex );
	
	Double_t EMu = 3.; // total energy [GeV]
	Double_t MMu = 105.658e-3; // muon mass [GeV/c^2]
	Double_t PMu = TMath::Sqrt(MMu*MMu+EMu*EMu);
	
	Double_t px=ux*PMu, py=-uy*PMu, pz=uz*PMu;
	
	Int_t mu = (gRandom->Rndm() > 0.5)?  MUP:MUM; // 5=mu+  6=mu-

        fStack->PushTrack(toBeDone, -1, mu, px, py, pz, EMu, vx, vy, vz, tof, 
			0, 0, 0, 
			kPPrimary, ntr, 1., 0);
	delete fcos;
      }


      return std::make_pair(-1,-1);
    }

  if(0) //test pions
    {
      //Double_t phi = gRandom->Rndm() * 2*3.14159265;
      //Double_t phi = 0.5*TMath::Pi();
      Double_t phi = TMath::Pi();
      Double_t Cph = TMath::Cos(phi);
      Double_t Sph = TMath::Sin(phi);
      
      Double_t radii = 2.2275;
      if(0)
        {   
          vx= radii*Cph;
          vy= radii*Sph;
          vz= gRandom->Gaus(-5.,2.5);
        }
      if(0)  // for calculating the z efficiency
        {
          vx = 1;
          vy = 0;
          vz = -5.5;
        }
      if(1) // for test
        {   
          vx= radii*Cph;
          vy= radii*Sph;
          vz= +7.5;
        }
      
      //
      TVector3 MCVertex( vx, vy, vz );
      gEvent->SetMCVertex( MCVertex );
      gEvent->SetCosmic( kFALSE );
            
      for (Int_t n=0; n<1; n++) 
        {
          // Lorentz Vector corresponding to the Nth decay
          //TVector3 *p = new TVector3(0.2,1.1,0.2);
	  TVector3 *p = new TVector3(0.,1.,0.);
          Double_t E = 2.*938.279E-3/1000.;
          
          // Particle type
          Int_t pdg  = +211;
          
          // Add particle to stack 
          fStack->PushTrack(toBeDone, -1, pdg, p->X(), p->Y(), p->Z(), E, vx, vy, vz, tof, 
                            0, 0, 0, 
                            kPPrimary, ntr, 1., 0);
        }
      return std::make_pair(-1,-1);
    }
    
  if(0) // test muon
  {
      //Double_t radii = 2.2275;
      Double_t radii = 7.48;
      //Double_t phi = 0.5*TMath::Pi()-0.01;
      Double_t phi = 0.05;
      vx = radii*TMath::Cos(phi);
      vy = radii*TMath::Sin(phi);
      vz = -5.5;      
      //
      TVector3 MCVertex( vx, vy, vz );
      gEvent->SetMCVertex( MCVertex );
      gEvent->SetCosmic( kFALSE );
      // Lorentz Vector corresponding to the Nth decay
      TVector3 *p = new TVector3(1.,0.,0.);
      Double_t E = 2.*938.279E-3/1000.;
      // Particle type
      Int_t pdg  = MUP;
      // Add particle to stack 
      fStack->PushTrack(toBeDone, -1, pdg, p->X(), p->Y(), p->Z(), E, vx, vy, vz, tof, 0, 0, 0, kPPrimary, ntr, 1., 0);
      fStack->PushTrack(toBeDone, -1, pdg, p->X(), p->Y(), p->Z(), E, vx, vy, -vz, tof, 0, 0, 0, kPPrimary, ntr, 1., 0);

      return std::make_pair(-1,-1);
   }

  if( 1 ) //pbar annihilation
    {
      Double_t phi = gRandom->Uniform(0.,TMath::TwoPi());
      Double_t Cph = TMath::Cos(phi);
      Double_t Sph = TMath::Sin(phi);
      
      Double_t radii = 2.2275;
      if(0)
        {   
          vx= 0.01;
          vy= 0.01;
          vz= 0.01;
        }
      if( 1 ) // mixing / trapping
        {   
          vx= radii*Cph;
          vy= radii*Sph;
          //vz= gRandom->Gaus(0.,2.5);
	  vz= gRandom->Gaus(0.,0.6); //a2mc
        }
      if( 0 )  // for calculating the z efficiency
        {
          vx = radii*Cph;
          vy = radii*Sph;
          vz = gRandom->Uniform(-30.,30.);
        }
      if(0) // to compare to new code a2mc
	{
	      Double_t zgen = 0.; 
	      Double_t xySig = 0.1, zSig = 0.6;
	      vx = gRandom->Gaus(0.,xySig);
	      vy = gRandom->Gaus(0.,xySig);
	      vz = zgen + gRandom->Gaus(0.,zSig);
	}
      if(0) // octupole
	{
          double Offz = 0;

          double HotSpotNum, HotSpotPhi, HotSpotCosPhi, HotSpotSinPhi;
          //HotSpotNum = (int) floor(8*gRandom->Rndm()); // This is from 0 to 7.
	  HotSpotNum = gRandom->Integer(8);

          if( ((int)HotSpotNum % 2) == 0 ) // even
          {
            HotSpotPhi = 2.*TMath::Pi()/8.*HotSpotNum;
            HotSpotCosPhi = TMath::Cos(HotSpotPhi);
            HotSpotSinPhi = TMath::Sin(HotSpotPhi);

            Offz -= 0.;
          }
          else
          {
            HotSpotPhi = 2.*TMath::Pi()/8.*HotSpotNum;
            HotSpotCosPhi = TMath::Cos(HotSpotPhi);
            HotSpotSinPhi = TMath::Sin(HotSpotPhi);

            Offz -= 6.;
          }

          vx = 2.2275*HotSpotCosPhi;
          vy = 2.2275*HotSpotSinPhi;
          vz = Offz;// gRandom->Gaus(Offz, Zsigma/2.);
	}
      if( 0 ) // FRD
	{
	  gFRDtree->GetEntry(gEntry);
	  vx = (Double_t) gFRDx;
          vy = (Double_t) gFRDy;
          vz = (Double_t) gFRDz;
	}
      
      //
      TVector3 MCVertex( vx, vy, vz );
      gEvent->SetMCVertex( MCVertex );
      gEvent->SetCosmic( kFALSE );
      
      Double_t nor=0;
      for (Int_t k=0;k<NFS;k++) nor+=BraRat[k];
      for (Int_t k=0;k<NFS;k++) BraRat[k] /= nor;
      
      BraRatCum[0] = BraRat[0];
      for (Int_t k=1;k<NFS;k++) BraRatCum[k] = BraRat[k] + BraRatCum[k-1];
      
      Float_t fs = gRandom->Rndm();
      Int_t fFs = 0;
      while (fs>BraRatCum[fFs]) fFs++;
      
      TLorentzVector P(0.,0.,0., 2.*938.279E-3);
      TGenPhaseSpace ps;     
      ps.SetDecay(P,NumPar[fFs],ParMas[fFs]);
      //printf("ps max: %lf MaxWeight %lf\n",ps.GetWtMax(),MaxWeigth[fFs]);
      
      Double_t wt=0.;
      while (1) // make unweighted events
	{
	  wt = ps.Generate(); // generate a phasespace configuration
	  Double_t wt_r = MaxWeigth[fFs]*gRandom->Rndm(); // random number between
	                                                  // [0,MaxWeight]
	  //printf("wt: %lf wt_r: %lf\n",wt,wt_r);
	  if(wt > wt_r) break;
	}
	  
      for (Int_t n=0; n<NumPar[fFs]; n++) 
        {
          // Lorentz Vector corresponding to the Nth decay
          TVector3 *p = new TVector3(ps.GetDecay(n)->Vect());
          Double_t E = ps.GetDecay(n)->E();
          
          // Particle type
          Int_t pdg  = ParNum[fFs][n];

          // Add particle to stack 
          fStack->PushTrack(toBeDone, -1, pdg, p->X(), p->Y(), p->Z(), E, vx, vy, vz, tof, 
                            0, 0, 0, 
                            kPPrimary, ntr, 1., 0);
        }
      return std::make_pair(fFs,wt);
  }
  return std::make_pair(-1,-1);
}

//
// public methods
//

//_____________________________________________________________________________
void TAlphaPrimaryGenerator::GeneratePrimaries()
{    
/// Fill the user stack (derived from TVirtualMCStack) with primary particle

  for (Int_t i=0; i<fNofPrimaries; i++) GeneratePrimary();  
}

//_____________________________________________________________________________
void TAlphaPrimaryGenerator::SetDirection(
                              Double_t dirX, Double_t dirY, Double_t dirZ) 
{ 
/// Set normalized direction
/// \param dirX  The new direction - x component
/// \param dirY  The new direction - y component
/// \param dirZ  The new direction - z component

  Double_t norm = TMath::Sqrt(dirX*dirX + dirY*dirY + dirZ*dirZ);
  
  fDirX = dirX/norm; 
  fDirY = dirY/norm;  
  fDirZ = dirZ/norm;
}
