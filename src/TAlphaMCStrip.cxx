//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TAlphaMCStrip                                                        //
//                                                                      //
// Monte Carlo Strip
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TAlphaMCStrip.h"

ClassImp(TAlphaMCStrip);

//______________________________________________________________________________
TAlphaMCStrip::TAlphaMCStrip()
{	
  fModule = -999;
  fASIC = -1;
  fStrip = -1;
  fADC = -999.;
  fFrac = -999.;
}

//______________________________________________________________________________
TAlphaMCStrip::TAlphaMCStrip( Int_t Module, Int_t ASIC, Int_t Strip, Double_t ADC, Double_t Frac )
{
  fModule = Module;
  fASIC = ASIC;
  fStrip = Strip;
  fADC = ADC;
  fFrac = Frac;
}

//______________________________________________________________________________
TAlphaMCStrip::TAlphaMCStrip( TAlphaMCStrip* &strip )
{
  fModule = strip->GetModule();
  fASIC   = strip->GetASIC();
  fStrip  = strip->GetStrip();
  fADC    = strip->GetADC();
  fFrac   = strip->GetFrac();
}

//______________________________________________________________________________
TAlphaMCStrip::~TAlphaMCStrip()
{
}
