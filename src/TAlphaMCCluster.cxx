//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TAlphaMCCluster                                                        //
//                                                                      //
// Monte Carlo Cluster
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TAlphaMCCluster.h"

ClassImp(TAlphaMCCluster);

//______________________________________________________________________________
TAlphaMCCluster::TAlphaMCCluster()
{
  fModule = -1;
  fASIC = -1;
  fNStrips = -1;
}

//______________________________________________________________________________
TAlphaMCCluster::TAlphaMCCluster( Int_t Module, Int_t ASIC, Int_t NStrips, Double_t Edep )
{
  fModule = Module;
  fASIC = ASIC;
  fNStrips = NStrips;
  fEdep = Edep;
}

//______________________________________________________________________________
TAlphaMCCluster::TAlphaMCCluster( TAlphaMCCluster* &cluster )
{
  fModule = cluster->GetModule();
  fASIC   = cluster->GetASIC();
  fNStrips = cluster->GetNStrips();
  fEdep = cluster->GetEdep();
}

//______________________________________________________________________________
TAlphaMCCluster::~TAlphaMCCluster()
{
}
