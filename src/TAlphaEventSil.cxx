#include <iostream>
#include <assert.h>

#include <TMath.h>

#include "TAlphaEvent.h"
#include "TAlphaEventSil.h"

ClassImp(TAlphaEventSil);

//////////////////////////////////////////////////////////////////////
//
//  TAlphaEventSil  
//
//////////////////////////////////////////////////////////////////////
//____________________________________________________________________
TAlphaEventSil::TAlphaEventSil(Char_t *silname) 
  : TAlphaEventObject(silname,1) 
{
  memset(fADCp,0,sizeof(fADCp));
  memset(fADCn,0,sizeof(fADCn));
  memset(fASIC1,0,sizeof(fASIC1));
  memset(fASIC2,0,sizeof(fASIC2));
  memset(fASIC3,0,sizeof(fASIC3));
  memset(fASIC4,0,sizeof(fASIC4));
  
  memset(fRMSn,0,sizeof(fRMSn));
  memset(fRMSp,0,sizeof(fRMSp));
  memset(fRMS1,0,sizeof(fRMS1));
  memset(fRMS2,0,sizeof(fRMS2));
  memset(fRMS3,0,sizeof(fRMS3));
  memset(fRMS4,0,sizeof(fRMS4));
  
  fHits.SetOwner(kTRUE);
  fHits.Clear();
  fNClusters.SetOwner(kTRUE);
  fNClusters.Clear();
  fPClusters.SetOwner(kTRUE);
  fPClusters.Clear();
}

//______________________________________________________________________________
TAlphaEventSil::TAlphaEventSil(const int num) 
  : TAlphaEventObject(num,1)
{
  memset(fADCp,0,sizeof(fADCp));
  memset(fADCn,0,sizeof(fADCn));
  memset(fASIC1,0,sizeof(fASIC1));
  memset(fASIC2,0,sizeof(fASIC2));
  memset(fASIC3,0,sizeof(fASIC3));
  memset(fASIC4,0,sizeof(fASIC4));	

  memset(fRMSn,0,sizeof(fRMSn));
  memset(fRMSp,0,sizeof(fRMSp));
  memset(fRMS1,0,sizeof(fRMS1));
  memset(fRMS2,0,sizeof(fRMS2));
  memset(fRMS3,0,sizeof(fRMS3));
  memset(fRMS4,0,sizeof(fRMS4));
  fHits.SetOwner(kTRUE);
  fHits.Clear();
  fNClusters.SetOwner(kTRUE);
  fNClusters.Clear();
  fPClusters.SetOwner(kTRUE);
  fPClusters.Clear();
}


//____________________________________________________________________
TAlphaEventSil::~TAlphaEventSil() 
{
  if (fHits.GetEntriesFast()>0)
  {
    fHits.SetOwner();
    fHits.Clear();
  }
  fNClusters.SetOwner();
  fPClusters.SetOwner();
  
  fNClusters.Delete();
  fPClusters.Delete();
}

//____________________________________________________________________
void TAlphaEventSil::GetStrippStartEnd (Int_t n, TVector3 &a, TVector3 &b) 
{
  // Provide positions for both ends of the pside strips for plotting 
  assert( n >= 0 );
  assert( n < 256);
	
  Double_t x = +SilX()/2.;
  Double_t y = GetpPos(n);
  Double_t z = SilZ()/2.;

  Double_t cos = GetCos();
  Double_t sin = GetSin();

  a.SetX(x*cos - y*sin + GetXCenter());
  a.SetY(x*sin + y*cos + GetYCenter());
  a.SetZ(z + GetZCenter()); 

  x = +SilX()/2.;
  y = GetpPos(n);
  z = -SilZ()/2.;
  
  b.SetX(x*cos - y*sin + GetXCenter());
  b.SetY(x*sin + y*cos + GetYCenter());
  b.SetZ(z + GetZCenter()); 

}

//____________________________________________________________________
void TAlphaEventSil::GetStripnStartEnd(Int_t n, TVector3 &a, TVector3 &b) 
{
  // Provide positions for both of the nside strips for plotting
  assert( n >= 0 );
  assert( n < 256);
	
  Double_t x = -SilX()/2.;
  Double_t y =  SilY()/2.;
  Double_t z = GetnPos(n);
  
  Double_t cos = GetCos();
  Double_t sin = GetSin();

  a.SetX(x*cos - y*sin + GetXCenter());
  a.SetY(x*sin + y*cos + GetYCenter());
  a.SetZ(z); 
  
  x = -SilX()/2.;
  y = -SilY()/2.;
  z = GetnPos(n);
  
  b.SetX(x*cos - y*sin + GetXCenter());
  b.SetY(x*sin + y*cos + GetYCenter());
  b.SetZ(z);
}

//____________________________________________________________________
void TAlphaEventSil::AddMCHitMRS(Double_t,Double_t, Double_t, Double_t) 
{
  
  printf("Debugging: TAlphaEventSil::AddMCHitMRS disabled!");
  assert(0);

/*
  // return to the local coordinates of the silicon module
  x = x - GetXCenter();
  y = y - GetYCenter();
  z = z;

  gEvent->GetVerbose()->Message("AddMCHitMRS",
                                "xyz: %lf %lf %lf s: %lf c: %lf\n",
                                x,y,z,GetSin(),GetCos());
  
  y = -x*GetSin() + y*GetCos();

  // Find the corresponding silicon strip
  Int_t pstrip = ReturnPStrip ( y );
  Int_t nstrip = ReturnNStrip ( z );
  if( (pstrip < 0) || (nstrip < 0) ||
      (pstrip > 255) || (nstrip > 255 ))
    {
      gEvent->GetVerbose()->Error("TAlphaEventSil::AddMCHitMRS",
                                  "Strip digitization error - p: %d n: %d %s\n",
                                  pstrip,nstrip,GetName());
      return;
    }
    
  //WARNING :: CHANGING ADC SCALE!!!
  Double_t adc_scale = 1.e6;
  adc = adc_scale * adc;
  
  //if( adc < 10. ) return;

  assert( pstrip >= 0 ); // shouldn't happen
  assert( pstrip < 256 );
  assert( nstrip >= 0 );
  assert( nstrip < 256 );

  //printf("n: %d, p: %d, adc: %lf xyz: %lf %lf %lf npos: %lf ppos: %lf\n",
  //       nstrip,pstrip,adc,x,y,z,GetnPos(nstrip),GetpPos(pstrip));

  Double_t fp = ( GetpPos( pstrip ) - y );
  Double_t fn = ( GetnPos( nstrip ) - z );

  if( fp < 1.e-32 ) fp = 0.;
  if( fn < 1.e-32 ) fn = 0.;
  if( fp > 1.e+32 ) fp = 1.;
  if( fn > 1.e+32 ) fn = 1.;
//AO  
  if( GetSilNum() >= nSil/2 )
    {
      fp *= -1.;
      fn *= -1.;
    }
  //printf("fp: %lf, (1-fp): %lf adc: %lf\n",fp,(1.-fabs(fp)),adc);
  
  if( nstrip > 127 )
    fASIC1[nstrip-128] += (1.-fabs(fn))*adc;
  else
    fASIC2[nstrip]     += (1.-fabs(fn))*adc;
  
  if( pstrip <= 127 )
    fASIC3[pstrip]     += (1.-fabs(fp))*adc;
  else 
    fASIC4[pstrip-128] += (1.-fabs(fp))*adc;
  
  // charge sharing
  if ( nstrip > 128 && nstrip < 255 )
    {
      Int_t dnstrip = nstrip + int(TMath::Sign(1.,fn));
      fASIC1[dnstrip-128] += fabs(fn)*adc;
    }
  if ( nstrip > 0 && nstrip < 127 )
    {
      Int_t dnstrip = nstrip + int(TMath::Sign(1.,fn));
      fASIC2[dnstrip] += fabs(fn)*adc;
    }
  if ( pstrip > 0 && pstrip < 127 )
    {
      Int_t dpstrip = pstrip + int(TMath::Sign(1.,fp));
      fASIC3[dpstrip] += fabs(fp)*adc;
    }
  if ( pstrip > 128 && pstrip < 255 )
    {
      Int_t dpstrip = pstrip + int(TMath::Sign(1.,fp));
      fASIC4[dpstrip-128] += fabs(fp)*adc;
    }

  // for( Int_t i = 0; i < 128; i++ )
  //   {
  //     fASIC1[i] = 0.;
  //     fASIC2[i] = 0.;
  //     fASIC3[i] = 0.;
  //     fASIC4[i] = 0.;
  //   }
  
  gEvent->GetVerbose()->Message("TAlphaEventSil::AddMCHitMRS",
                                "End function\n");
*/
} 



//______________________________________________________________________________
void TAlphaEventSil::AddMCHit( Double_t en_x, Double_t en_y, Double_t en_z,
                               Double_t ex_x, Double_t ex_y, Double_t ex_z,
                               Double_t edep,
                               TObjArray * strips )
{
// Digitization routines
// MUST be reviewed

  //printf("Digi begin...\n");

  Int_t p_start_strip = -1;
  Int_t p_end_strip = -1;
  Int_t n_start_strip = -1;
  Int_t n_end_strip = -1;
  Double_t p_start_pos = 0.;
  Double_t p_end_pos = 0.;
  Double_t n_start_pos = 0.;
  Double_t n_end_pos = 0.;

  // change adc scale GeV -> keV
  Double_t adc = edep * 1.e6;

  // return to the local coordinates of the silicon module
  Double_t en_x_prime = en_x - GetXCenter();
  Double_t en_y_prime = en_y - GetYCenter();
  Double_t en_z_prime = en_z;
   
  Double_t en_p = -en_x_prime * GetSin() 
                  +en_y_prime * GetCos();
  Double_t en_n =  en_z_prime;


  Double_t ex_x_prime = ex_x - GetXCenter();
  Double_t ex_y_prime = ex_y - GetYCenter();
  Double_t ex_z_prime = ex_z;
    
  Double_t ex_p = -ex_x_prime * GetSin()
                  +ex_y_prime * GetCos();
  Double_t ex_n =  ex_z_prime;

//   printf("particle entering\n");
//   printf("en: %lf %lf %lf\n",en_x,en_y,en_z);
//   printf("n: %lf\n",en_n);
//   printf("p: %lf\n",en_p);
  Int_t en_p_s = ReturnPStrip( en_p );
  Int_t en_n_s = ReturnNStrip( en_n );
//   printf("particle exiting\n");
//   printf("ex: %lf %lf %lf\n",ex_x,ex_y,ex_z);
//   printf("n: %lf\n",ex_n);
//   printf("p: %lf\n",ex_p);
  Int_t ex_p_s = ReturnPStrip( ex_p );
  Int_t ex_n_s = ReturnNStrip( ex_n );

  // decide which are starting strips, and which are ending strips
  // start < end
  p_start_strip = ( en_p_s > ex_p_s ? ex_p_s : en_p_s ); 
  p_end_strip   = ( en_p_s > ex_p_s ? en_p_s : ex_p_s );
  n_start_strip = ( en_n_s > ex_n_s ? ex_n_s : en_n_s );
  n_end_strip   = ( en_n_s > ex_n_s ? en_n_s : ex_n_s );

  p_start_pos   = ( en_p_s > ex_p_s ? ex_p   : en_p );
  p_end_pos     = ( en_p_s > ex_p_s ? en_p   : ex_p );
  n_start_pos   = ( en_n_s > ex_n_s ? ex_n   : en_n );
  n_end_pos     = ( en_n_s > ex_n_s ? en_n   : ex_n );

  //'printf("1: %d %d %d %d\n",p_start_strip,p_end_strip,n_start_strip,n_end_strip);

  // catostrophic strip determination fail
  if( n_start_strip == N_FAIL() || n_end_strip == N_FAIL() )
    {
      printf("N-side digitization Error -- %d %d %s %lf %lf\n",
	     n_start_strip,n_end_strip,GetName(),en_n,ex_n);
      return;
    } 

  // conditions where both the starts and ends are outside the strip region
  if( n_start_strip == N_LEFT() || n_end_strip == N_LEFT() ){
   //  printf("n_start_strip = %d || n_end_strip = %d\n",n_start_strip,n_end_strip);
    return;}
  if( n_start_strip == N_RIGHT() || n_end_strip == N_RIGHT() ){
   //  printf("n_start_strip = %d || n_end_strip = %d\n",n_start_strip,n_end_strip);
    return;}
  if( n_start_strip == N_MIDDLE() || n_end_strip == N_MIDDLE() ){
   //  printf("n_start_strip = %d || n_end_strip = %d\n",n_start_strip,n_end_strip);
    return;}

  if( n_start_strip > n_end_strip )
    {
      Int_t tmp  = n_start_strip;
      Double_t tmp_pos = n_start_pos;
      n_start_strip = n_end_strip;
      n_start_pos   = n_end_pos;
      n_end_strip = tmp;
      n_end_pos   = tmp_pos;
    }

  assert( n_start_strip <= n_end_strip );
  
  // distribute between the strips based on the fraction of the hit
  // located underneath any given strip
  //printf("nlength: %d\n",(n_end_strip - n_start_strip + 1));
  assert( (n_end_strip - n_start_strip + 1) > 0 );
  
  Double_t de_n[n_end_strip - n_start_strip + 1];
  Double_t fe_n[n_end_strip - n_start_strip + 1];
  memset( de_n, 0, sizeof(de_n) );
  memset( fe_n, 0, sizeof(fe_n) );

  if( n_end_strip - n_start_strip > 0 )
    {
      Double_t tot = 0;
      for( Int_t n = n_start_strip; n <= n_end_strip; n++ )
        {
	  //printf("N: %d\n",n);
          Double_t n_pos = GetnPos( n );
          if( n == n_start_strip )
            {
              // printf("s: %lf %lf %lf fe: %lf\n",n_start_pos,n_pos,n_start_pos-n_pos,
	      // 	     (n_start_pos - n_pos + 0.5*0.0875)/0.0875);
	      fe_n[n-n_start_strip] = (n_start_pos - n_pos + 0.5*SilNPitch())/SilNPitch();
              tot += fe_n[n-n_start_strip];
            }
          else if ( n == n_end_strip )
            {
              // printf("e: %lf %lf %lf fe: %lf\n",n_end_pos,n_pos,n_end_pos-n_pos,
	      // 	     (n_pos - n_end_pos + 0.5*0.0875)/0.0875);
	      fe_n[n-n_start_strip] = (n_pos - n_end_pos + 0.5*SilNPitch())/SilNPitch();
              tot += fe_n[n-n_start_strip];
            }
          else
            {
              fe_n[n-n_start_strip] = 1.;
              tot += 1.;
            }

	  assert( fe_n[n-n_start_strip] >= 0 );
	  assert( fe_n[n-n_start_strip] <= 1.0 );
          //printf("n: %d  fe_n: %lf tot: %lf\n",n,fe_n[n-n_start_strip],tot);	  
        }
      for( Int_t n = n_start_strip; n <= n_end_strip; n++ )
        {
          de_n[n-n_start_strip] = fe_n[n-n_start_strip]/tot;
        }
    }
  else
    de_n[0] = 1.;

// pside:

  if( p_start_strip == P_FAIL() ||
      p_end_strip   == P_FAIL() )
    {
      printf("P-side digitization Error -- %d %d %s %lf %lf\n",p_start_strip,p_end_strip,
	     GetName(),en_p,ex_p);
      return;
    }

// both start and end are outside the strip region
  if( p_start_strip == P_LEFT() || p_end_strip == P_LEFT() ){
    // printf("p_start_strip = %d || p_end_strip = %d\n",p_start_strip,p_end_strip);
    return;}
  if( p_start_strip == P_RIGHT() || p_end_strip == P_RIGHT() ){
   //  printf("p_start_strip = %d || p_end_strip = %d\n",p_start_strip,p_end_strip);
    return;}

  //printf("2: %d %d %d %d\n",p_start_strip,p_end_strip,n_start_strip,n_end_strip);

  if( p_start_strip > p_end_strip )
    {
      Int_t tmp  = p_start_strip;
      Double_t tmp_pos = p_start_pos;
      p_start_strip = p_end_strip;
      p_start_pos   = p_end_pos;
      p_end_strip = tmp;
      p_end_pos   = tmp_pos;
    }


  //printf("3: %d %d %d %d %lf %lf\n",p_start_strip,p_end_strip,n_start_strip,n_end_strip,p_start_pos,p_end_pos);

  assert( p_start_strip <= p_end_strip );

  if( p_start_strip < 0 || p_start_strip > 255 ) return;
  if( p_end_strip < 0 || p_end_strip > 255 ) return;
  
  // distribute between the strips based on the fraction of the hit
  // located underneath any given strip
  assert( (p_end_strip - p_start_strip + 1) > 0 );
  //printf("plength: %d\n",(p_end_strip - p_start_strip + 1));
  Double_t de_p[p_end_strip - p_start_strip + 1];
  Double_t fe_p[p_end_strip - p_start_strip + 1];
  memset( de_p, 0, sizeof(de_p) );
  memset( fe_p, 0, sizeof(fe_p) );

  if( p_end_strip - p_start_strip > 0 )
    {
      Double_t tot = 0;
      for( Int_t p = p_start_strip; p <= p_end_strip; p++ )
	{
	  //printf("P: %d\n",p);
	  Double_t p_pos = GetpPos( p );
	  if( p == p_start_strip )
	    {
	      //printf("s: %lf %lf %lf\n",p_start_pos,p_pos,p_start_pos-p_pos);
	      fe_p[p-p_start_strip] = (p_pos - p_start_pos + 0.5*SilPPitch())/SilPPitch();
	      tot += fe_p[p-p_start_strip];
	    }
	  else if ( p == p_end_strip )
	    {
	      //printf("e: %lf %lf %lf\n",p_end_pos,p_pos,p_end_pos-p_pos);
	      fe_p[p-p_start_strip] = (p_end_pos - p_pos + 0.5*SilPPitch())/SilPPitch();
	      tot += fe_p[p-p_start_strip];
	    }
	  else
	    {
	      fe_p[p-p_start_strip] = 1.;
	      tot += 1.;
	    }
	  //printf("p: %d  fe_p: %lf tot: %lf\n",p,fe_p[p-p_start_strip],tot);

	  assert( fe_p[p-p_start_strip] >= 0 );
	  assert( fe_p[p-p_start_strip] <= 1.0 );
	}
      for( Int_t p = p_start_strip; p <= p_end_strip; p++ )
	{
	  de_p[p-p_start_strip] = fe_p[p-p_start_strip]/tot;
	  
	}
    }
  else
    de_p[0] = 1.;
    
    // fine hit sharing
    
    
    
    
    
    // inizio digitizzazione
    Double_t frac = 0.4;
    
  // calculate adc values, create the strip objects, fill the strip array   
   for( Int_t n = n_start_strip; n <= n_end_strip; n++ )
    {
      if( de_n[n-n_start_strip] < frac ) continue;

      Double_t s_adc = adc*de_n[n-n_start_strip];
      //printf("digi n: %d e: %lf %lf\n",n,adc,s_adc);

      if( n < 128 )
        {
          fASIC1[n] += s_adc;
          TAlphaMCStrip * s = new TAlphaMCStrip( GetSilNum(), 1, n, s_adc, fe_n[n-n_start_strip] );
          strips->AddLast(s);
        }
      else
        {
          fASIC2[n-128] += s_adc;
          TAlphaMCStrip * s = new TAlphaMCStrip( GetSilNum(), 2, n-128, s_adc, fe_n[n-n_start_strip] );
          strips->AddLast(s);
        }
    }

  for( Int_t p = p_start_strip; p <= p_end_strip; p++ )
    {
     // printf("%d    fep=%lf   dep=%lf\n",p,fe_p[p-p_start_strip],de_p[p-p_start_strip]);
      if( de_p[p-p_start_strip] < frac ) continue;

      Double_t s_adc = adc*de_p[p-p_start_strip];
      //printf("digi p: %d e: %lf %lf\n",p,adc,s_adc);
      
      if( p < 128 )
        {
          fASIC3[p] += s_adc;
          TAlphaMCStrip * s = new TAlphaMCStrip( GetSilNum(), 3, p, s_adc, fe_p[p-p_start_strip] );
          strips->AddLast(s);
        }
      else
        {
          fASIC4[p-128] += s_adc;
          TAlphaMCStrip * s = new TAlphaMCStrip( GetSilNum(), 4, p-128, s_adc, fe_p[p-p_start_strip] );
          strips->AddLast(s);
        }
    }

   // printf("Digi end\n");
}
//____________________________________________________________________
void TAlphaEventSil::AddMCStrips(Double_t en_x, Double_t en_y, Double_t en_z,
				 Double_t ex_x, Double_t ex_y, Double_t ex_z, 
				 Double_t edep, TObjArray * strips )
{
  //  printf("DIGI START\n");
  Double_t conv = 2.40E+006; // [mV/GeV] guess!
  Double_t share_frac_threshold=0.3; // arbitrary!

  // return to the local coordinates of the silicon module
  Double_t en_n =  en_z;
  Double_t ex_n =  ex_z;

  // position to strip number
  Int_t n1_tilde = ReturnNStrip(en_n);
  if(boundary_flag(n1_tilde)) return; // exit on strip finding failure
  Int_t n2_tilde = ReturnNStrip(ex_n);
  if(boundary_flag(n2_tilde)) return;

  // return to the local coordinates of the silicon module
  Double_t en_x_prime = en_x - GetXCenter();
  Double_t en_y_prime = en_y - GetYCenter();   
  Double_t en_p = -en_x_prime * GetSin() + en_y_prime * GetCos();

  Double_t ex_x_prime = ex_x - GetXCenter();
  Double_t ex_y_prime = ex_y - GetYCenter();    
  Double_t ex_p = -ex_x_prime * GetSin() + ex_y_prime * GetCos();

  Int_t p1_tilde = ReturnPStrip(en_p);
  if(boundary_flag(p1_tilde)) return; // exit on strip finding failure
  Int_t p2_tilde = ReturnPStrip(ex_p);
  if(boundary_flag(p2_tilde)) return;

  //printf("n1~ = %d  n2~ = %d  z_n = %lf  z_x = %lf\n",n1_tilde,n2_tilde,en_n,ex_n);
  //printf("p1~ = %d  p2~ = %d  y_n = %lf  y_x = %lf\n",p1_tilde,p2_tilde,en_p,ex_p);

  // middle point
  Double_t m_n = (en_n+ex_n)*0.5;
  Double_t m_p = (en_p+ex_p)*0.5;

  // add a second strip
  if(n1_tilde==n2_tilde) 
    {
      Int_t nplus=ReturnNStrip(m_n+SilNPitch());
      Int_t nminus=ReturnNStrip(m_n-SilNPitch());
      if(boundary_flag(nplus) && !boundary_flag(nminus))
	n2_tilde = nminus;
      else if(!boundary_flag(nplus) && boundary_flag(nminus))
	n2_tilde = nplus;
      else if (!boundary_flag(nplus) && !boundary_flag(nminus))
	n2_tilde = TMath::Abs(GetnPos(nplus)-m_n) < TMath::Abs(GetnPos(nminus)-m_n)? nplus : nminus;
      assert(TMath::Abs(n1_tilde-n2_tilde)==1); // debug 1
    }

  if(p1_tilde==p2_tilde) 
    {
      Int_t pplus =ReturnPStrip(m_p+SilPPitch());
      Int_t pminus=ReturnPStrip(m_p-SilPPitch());
      if(boundary_flag(pplus) && !boundary_flag(pminus))
	p2_tilde = pminus;
      else if(!boundary_flag(pplus) && boundary_flag(pminus))
	p2_tilde = pplus;
      else if (!boundary_flag(pplus) && !boundary_flag(pminus))
	p2_tilde = TMath::Abs(GetpPos(pplus)-m_p) < TMath::Abs(GetpPos(pminus)-m_p)? pplus : pminus;
      assert(TMath::Abs(p1_tilde-p2_tilde)==1); // debug 1
    }

  Int_t n1=-999.,n2=-999,p1=-999.,p2=-999.;
 
  // order strips for convenience
    n1=n1_tilde<n2_tilde?n1_tilde:n2_tilde;
    n2=n1_tilde<n2_tilde?n2_tilde:n1_tilde;
    assert(n1<n2); //debug 2
    p1=p1_tilde<p2_tilde?p1_tilde:p2_tilde;
    p2=p1_tilde<p2_tilde?p2_tilde:p1_tilde;
    assert(p1<p2); //debug 2
    //printf("n1 = %d  n2 = %d    zm = %lf\n",n1,n2,m_n);
    //printf("p1 = %d  p2 = %d    ym = %lf\n",p1,p2,m_p);

    // calculate total
    Double_t* zpos = new Double_t[n2-n1+1];
    Double_t* ypos = new Double_t[p2-p1+1];
    Double_t totn=0.;
    Double_t totp=0.;
    for(Int_t n=n1; n<=n2; ++n)
      {
	zpos[n-n1]=GetnPos(n);
	totn+=TMath::Abs(zpos[n-n1]-m_n);
	//printf("n = %d   z = %lf    tot = %lf\n",n,zpos[n-n1],totn);
      }
    if(totn==0.) totn=1.;
    for(Int_t p=p1; p<=p2; ++p)
      {
	ypos[p-p1]=GetpPos(p);
	totp+=TMath::Abs(ypos[p-p1]-m_p);
	//printf("p = %d   y = %lf    tot = %lf\n",p,ypos[p-p1],totp);
      }
    if(totp==0.) totp=1.;

    Double_t adc=-999.,frac=-999.;

    // ADD N-TYPE STRIPS
     for(Int_t n=n1; n<=n2; ++n)
      {
	frac=TMath::Abs(zpos[n-n1]-m_n)/totn; // calculate charge fraction
	if(frac<share_frac_threshold) continue;
	adc=frac*edep*conv;
	// ADD STRIP
	if( n < 128 )
	  {
	    fASIC1[n] += adc;
	    TAlphaMCStrip * s = new TAlphaMCStrip( GetSilNum(), 1, n, adc, frac ); 
	    strips->AddLast(s);
	    //printf("digi n: %d  adc: %lf  frac: %lf\n",n,adc,frac);
	  }
	else
	  {
	    fASIC2[n-128] += adc;
	    TAlphaMCStrip * s = new TAlphaMCStrip( GetSilNum(), 2, n-128, adc, frac );
	    strips->AddLast(s);
	    //printf("digi n: %d  adc: %lf  frac: %lf\n",n,adc,frac);
	  }
      }

    // ADD P-TYPE STRIPS
    for(Int_t p=p1; p<=p2; ++p)
      {
	frac=TMath::Abs(ypos[p-p1]-m_p)/totp; // calculate charge fraction
	if(frac<share_frac_threshold) continue;
	adc=frac*edep*conv;
	// ADD STRIP
	if( p < 128 )
	  {
	    fASIC3[p] += adc;
	    TAlphaMCStrip * s = new TAlphaMCStrip( GetSilNum(), 3, p, adc, frac ); 
	    strips->AddLast(s);
	    //printf("digi p: %d  adc: %lf  frac: %lf\n",p,adc,frac);
	  }
	else
	  {
	    fASIC4[p-128] += adc;
	    TAlphaMCStrip * s = new TAlphaMCStrip( GetSilNum(), 4, p-128, adc, frac );
	    strips->AddLast(s);
	    //printf("digi p: %d  adc: %lf  frac: %lf\n",p,adc,frac);
	  }
      }
    
    delete[] zpos;
    delete[] ypos;
    //printf("DIGI STOP\n");
}
//____________________________________________________________________
Bool_t TAlphaEventSil::boundary_flag(Int_t f)
{
  for(Int_t i=-1; i>=-8; --i)
    if(f==i) return kTRUE;
  return kFALSE;
}

//____________________________________________________________________
Int_t TAlphaEventSil::GetOrPhi() {
	
  // Can have to p-side triggers from each module
  Int_t ta1 = 0;
  Int_t ta2 = 0;
  
  for(Int_t i=0;i<256;i++) 
    {
      if(fADCp[i] > 0.) 
        {
          if( i < 128 ) ta1 = 1;
          else ta2 = 1;
        }
    }

  //printf("ORPHI: %d\n",ta1+ta2);
  return ta1 + ta2;
}

//____________________________________________________________________
Int_t TAlphaEventSil::GetOrN() {
	
  // Can have to n-side triggers from each module
  Bool_t ta1 = false;
  Bool_t ta2 = false;
  
  for(Int_t i=0;i<256;i++) 
    if(fADCn[i]) 
      {
	if( i < 128 ) ta1 = true;
	else ta2 = true;
      }
  //printf("ORPHI: %d\n",ta1+ta2);
  return ta1 + ta2;
}

//______________________________________________________________________________
void TAlphaEventSil::MapASICtoStrips()
{
  // Take the 4 ASIC 128 arrays and make two 256 arrays
  
  for( Int_t i = 0; i < 128; i++)
    {
      fADCn[i]     = fASIC1[i];
      fADCn[i+128] = fASIC2[i];
      fADCp[i]     = fASIC3[i];
      fADCp[i+128] = fASIC4[i];
      
      
      fRMSn[i]     = fRMS1[i];
      fRMSn[i+128] = fRMS2[i];
      fRMSp[i]     = fRMS3[i];
      fRMSp[i+128] = fRMS4[i];
      
      
    }
}


//______________________________________________________________________________
void TAlphaEventSil::RecCluster()
{
  gEvent->GetVerbose()->Message("RecCluster",
                                "---Clustering %s (%d)---\n",
                                GetName(),GetSilNum());
  MapASICtoStrips();
  
  // pside
  Bool_t A = kFALSE;                                 // run-length encoding
  Int_t pBeg[256];
  Int_t pRun[256]; 
  Int_t N=0;
  for (Int_t k=0; k< 256; k++) 
    { 
      Bool_t B = fADCp[k];
      if (!A &&  B) { pBeg[N]=k; pRun[N]=1; }  // 01 = begin
      if ( A &&  B)   pRun[N]++;               // 11
      if ( A && !B)   N++;                     // 10 = end
      A = B;
    }
  if (A) N++; 
  Int_t Npside=N; 
  
  // nside
  A = kFALSE;                                 // run-length encoding
  Int_t nBeg[256];
  Int_t nRun[256]; 
  N=0;
  for (Int_t k=0; k< 256; k++) 
    { 
      Bool_t B = fADCn[k];
      if (!A &&  B) { nBeg[N]=k; nRun[N]=1; }  // 01 = begin
      if ( A &&  B)   nRun[N]++;               // 11
      if ( A && !B)   N++;                     // 10 = end
      A = B;
    }
  if (A) N++; 
  Int_t Nnside=N; 
  
  gEvent->GetVerbose()->Message("RecCluster",
                                "Nside: %d Pside: %d Total: %d\n",
                                Nnside,
                                Npside,
                                Nnside*Npside);
  
  for( Int_t inside = 0; inside < Nnside; inside++)
    {
      TAlphaEventNCluster * c = new TAlphaEventNCluster(GetSilNum());
      
      for (Int_t h=0; h<nRun[inside]; h++)
        {
          TAlphaEventNStrip * s = 
            new TAlphaEventNStrip(nBeg[inside]+h,fabs(fADCn[nBeg[inside]+h]),fRMSn[nBeg[inside]+h]);
          c->AddStrip( s );
        }
      c->Calculate();
      if (c->GetSigma() > gEvent->GetNClusterSigma() /*NGetNClusterSigma()*/) fNClusters.Add( c );
      else { c->Delete();}
    }
  
  for( Int_t ipside = 0; ipside < Npside; ipside++)
    {
      TAlphaEventPCluster * c = new TAlphaEventPCluster(GetSilNum());
      
      for (Int_t h=0; h<pRun[ipside]; h++)
        {
          TAlphaEventPStrip * s = 
            new TAlphaEventPStrip(pBeg[ipside]+h,fabs(fADCp[pBeg[ipside]+h]),fRMSp[pBeg[ipside]+h]);
          c->AddStrip( s );
        }
      c->Calculate();
      //c->Print();
      if (c->GetSigma() >  gEvent->GetPClusterSigma())
    
      fPClusters.Add( c );
      else { c->Delete();}
    }
}

void TAlphaEventSil::RemoveHit(TAlphaEventHit* remove)
{
	Int_t hits=0;
	if (fNClusters.IsEmpty() ) return;
	if (fPClusters.IsEmpty() ) return;
  for( Int_t in = 0; in < fNClusters.GetEntriesFast(); in++ )
  {
    for( Int_t ip = 0; ip < fPClusters.GetEntriesFast(); ip++ )
    {
      TAlphaEventNCluster * n = GetNCluster( in );
      if (!n) continue;
      //n->Print();
      TAlphaEventPCluster * p = GetPCluster( ip );
      if (!p) continue;
      //p->Print();
      TAlphaEventHit * h = new TAlphaEventHit( GetSilNum(), p,n );
      if ( h->Y() == remove->Y() && h->Z() == remove->Z())
      {
        n->Delete();
        p->Delete();
        fNClusters.RemoveAt(in);
        fPClusters.RemoveAt(ip);
        //std::cout <<"Removing hit"<<std::endl;
      }
         hits++;
   
    }
  }
  //std::cout<<"Pre hits:"<<hits<<std::endl;
  if (hits)
  {
    fNClusters.Compress();
    fPClusters.Compress();
    fHits.Clear();
    fHits.Compress();
    RecHit();
  }
}
//______________________________________________________________________________
void TAlphaEventSil::RecHit()
{
  //printf("%d NClusters: %d, PClusters: %d\n",GetSilNum(),fNClusters.GetEntriesFast(),fPClusters.GetEntriesFast());
  Int_t Hits=0;
 // Double_t Sig[10000];
  //Double_t Sig[fNClusters.GetEntriesFast()*fPClusters.GetEntriesFast()];
 /*  for( Int_t in = 0; in < fNClusters.GetEntriesFast(); in++ )
    {
      for( Int_t ip = 0; ip < fPClusters.GetEntriesFast(); ip++ )
        {
          TAlphaEventNCluster * n = GetNCluster( in );
          TAlphaEventPCluster * p = GetPCluster( ip );
          TAlphaEventHit * h = new TAlphaEventHit( GetSilNum(), p,n );
          //Sig[Hits]=h->GetHitSignifance();
          
          delete h;
        }
    }
   // std::cout<<"Hits:"<<Hits<<std::endl;
 Double_t SigCut=-1.; //Off by default
  if (Hits>gEvent->GetHitThreshold())
  {
    std::sort(Sig,Sig + Hits);
   
   // for (Int_t i = Hits; i> 0; i-- )
   // {
   //    std::cout <<Sig[i]<<std::endl;
   // }
    if (Hits>gEvent->GetHitThreshold() && Int_t(Hits-gEvent->GetHitThreshold())>0)
    {
      SigCut=Sig[(Int_t(Hits-gEvent->GetHitSignificance()))];
      std::cout <<"SigCut: "<<SigCut<<" at "<<
      (Int_t(Hits-gEvent->GetHitSignificance()))
      <<std::endl;
    }
  }*/
  for( Int_t in = 0; in < fNClusters.GetEntriesFast(); in++ )
    {
      for( Int_t ip = 0; ip < fPClusters.GetEntriesFast(); ip++ )
        {      
          TAlphaEventNCluster * n = GetNCluster( in );
          TAlphaEventPCluster * p = GetPCluster( ip );
             
          TAlphaEventHit * h = new TAlphaEventHit( GetSilNum(), p,n );
          //h->Print();
          //if (h->GetHitSignifance() < SigCut) delete h;
          //else AddHit( h );
          Hits++;
          AddHit( h );
        }
    }
}

void TAlphaEventSil::Print(Option_t*) const
{
  //  std::cout<<"TAlphaEventSil::Silicon #:"<<GetSilNum()<<" name: "<<ReturnSilName(GetSilNum())<<" layer: "<<GetLayer()<<std::endl;
  std::cout<<"TAlphaEventSil::Silicon #:"<<GetSilNum()<<" layer: "<<GetLayer()<<std::endl;
  for( int s=0; s<128; ++s)
    {
      std::cout<<s<<"\t"<<fASIC1[s]<<"\t"<<fASIC2[s]<<"\t"<<fASIC3[s]<<"\t"<<fASIC4[s]<<std::endl;
    }
}
