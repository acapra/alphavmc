// Description: relate outputs from VF48 to hits on the Si detectors
// Author: Art Olin 

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>

using std::ifstream;
using std::cout;
using std::string;
using std::getline;
using std::stringstream;

#include "../include/VF48SiMap.h"

VF48SiMap::VF48SiMap( const string& fname ) :
    maxVF48samples_(128)
{
  ifstream mapfile( fname.c_str() );
  if( !mapfile) {
    cout << "Map file " << fname << " not found\n";
    exit(EXIT_FAILURE);  // failure
  }
  //
  // initialize arrays
  for( int i=0; i<nSil; ++i )
  {
    for( int j=0; j<4; ++j )
    {
      module_[i][j] = -1;
      channel_[i][j] = -1;
      frcnumber_[i][j] = -1;
      frcport_[i][j] = -1;
      ttcchannel_[i][j] = -1;
    }
  }
  for (int i=0;i<nVF48;++i)
  {
  for (int j=0;j<48; j++)
    {
      sinumber_[i][j]  = -1;
      va_[i][j]  =-1;
      frcn_[i][j]  =-1;
      frcp_[i][j]  =-1;
      ttcc_[i][j]  =-1;
    }
   }
  //
  string buffer;
  while( !getline(mapfile,buffer).eof() )
  {
    if( buffer[0] == '#' )
    {
    //cout << buffer << "\n";
    }
    else 
    {
      int Module, Plug, SiNumber, FRCNumber, FRCPort, TTCChannel;
      string SiName;
      stringstream ss;
      ss << buffer;
      ss >> Module >> Plug >> FRCNumber >> FRCPort >> SiName >> SiNumber >> TTCChannel;

      //      cout << Module << " " << Plug << " " << FRCNumber << " " << FRCPort
      //  << " " << SiName << " " << SiNumber << " " << TTCChannel << "\n";

      for( int VA=0; VA<4; ++VA )
      {
        module_[SiNumber][VA] = Module;
        int channel =  VA+4*FRCPort+16*Plug;
        channel_[SiNumber][VA] = channel;
        frcnumber_[SiNumber][VA] = FRCNumber;
        frcport_[SiNumber][VA] = FRCPort;
        ttcchannel_[SiNumber][VA] = TTCChannel+VA;
        sinumber_[Module][channel]=SiNumber;
        siname_[Module][channel] = SiName;
        va_[Module][channel]=VA+1;
        frcn_[Module][channel] =FRCNumber;
        frcp_[Module][channel] =FRCPort;
        ttcc_[Module][channel] = TTCChannel+VA;
        
      }
      if (maxmodule_ < Module) maxmodule_ = Module;

      Siname_[SiNumber] = SiName;

    }
  }

  //debug
/*  for(int channel=0; channel<48; channel++)
    {
      std::cout << channel << "\t" << sinumber_[3][channel] << "\t" << va_[3][channel] << "\t" << frcn_[3][channel] << std::endl;
    } 
*/  

}

int VF48SiMap::GetVF48( const int SiNumber , int va,
                        int &Module, int &Channel, int &TTCChannel )
{
  if (SiNumber > nSil) exit(EXIT_FAILURE); 
  Module = module_[SiNumber][va-1];
  Channel = channel_[SiNumber][va-1];
  TTCChannel = ttcchannel_[SiNumber][va-1];
  return (EXIT_SUCCESS);
}

int VF48SiMap::GetSil ( int Module, const int Channel, int &SiNumber, int &VA,int &FRCNumber, int &FRCPort, int &TTCChannel )
{
  SiNumber = sinumber_[Module][Channel];
  VA=va_[Module][Channel];
  FRCNumber = frcn_[Module][Channel];
  FRCPort = frcp_[Module][Channel];
  TTCChannel = ttcc_[Module][Channel];
 return(EXIT_SUCCESS);
}

string VF48SiMap::GetSilName( int Module, int Channel)
{
  return siname_[Module][Channel];
}

string VF48SiMap::GetSilName( int SiNumber )
{
  return Siname_[SiNumber];
}

