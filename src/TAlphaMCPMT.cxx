//

#include "TAlphaMCPMT.h"

ClassImp( TAlphaMCPMT )

//_____________________________________________________________________________
TAlphaMCPMT::TAlphaMCPMT()
{
  fedep = 0.;
}

//_____________________________________________________________________________
TAlphaMCPMT::TAlphaMCPMT(Double_t e, Double_t time, const char* name)
{
  fedep = e;
  ftime = time;
  SetName(name);
}

//_____________________________________________________________________________
TAlphaMCPMT::~TAlphaMCPMT()
{
  //
}

//end
