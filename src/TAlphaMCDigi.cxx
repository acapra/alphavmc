//===========================================
// Digitization Class for ALPHA MC
// AndreaC  --- October 2012
//===========================================

#define VMC_STUDY 1

#include <iostream>
int NStep=0;

#include <TVirtualMC.h>

#include "TAlphaMCStrip.h"
#include "TAlphaMCCluster.h"
#include "TAlphaMCDigi.h"

ClassImp(TAlphaMCDigi)

TAlphaMCDigi::TAlphaMCDigi(TGeoVolume *CurrentVol, TAlphaEvent *Event, Double_t EnergyDeposit)
{
	fHybrid = new TAlphaEventSil((Char_t*)CurrentVol->GetName());
	Event->AddSil( fHybrid );
	
	fStrips = new TObjArray();
	
	fEnergyDeposit = EnergyDeposit;
	
	#if VMC_STUDY<1
	fEnergyThreshold=10.;
	#else
	fEnergyThreshold=0.;
	#endif
}

//_____________________________________________________________________________
TAlphaMCDigi::~TAlphaMCDigi()
{
	delete fHybrid;
	delete fStrips;
}

//_____________________________________________________________________________
void TAlphaMCDigi::SharedHit(Double_t EnPoint[],Double_t ExPoint[])
{
	// according to me, the factor 1.e6 means MeV -> eV
  if( gMC->IsTrackExiting() && ((fEnergyDeposit * 1.e6) > fEnergyThreshold) )
    //    fHybrid->AddMCHit( EnPoint[0], EnPoint[1], EnPoint[2],
    //		       ExPoint[0], ExPoint[1], ExPoint[2],
    //		       fEnergyDeposit, fStrips );
    fHybrid->AddMCStrips( EnPoint[0], EnPoint[1], EnPoint[2],
			  ExPoint[0], ExPoint[1], ExPoint[2],
			  fEnergyDeposit, fStrips );
			 
// std::cout<<"### "<<NStep<<"\t"<<cpoint[0]<<"\t"<<cpoint[1]<<"\t"<<cpoint[2]<<"\t"
// <<epoint[0]<<"\t"<<epoint[1]<<"\t"<<epoint[2]<<"\t"<<Edep* 1.e6<<std::endl;
// ++NStep;
}

//_____________________________________________________________________________
Int_t TAlphaMCDigi::CreateClusterOutput(TClonesArray *ClusterA, Int_t *NClusters)
{
	// according to me, the factor 1.e6 means MeV -> eV
	if( gMC->IsTrackExiting() && ((fEnergyDeposit * 1.e6) > fEnergyThreshold) )
	{
		// Add Clusters to Output Tree
		TAlphaMCStrip * cstrip = (TAlphaMCStrip*) fStrips->At(0);
		if(cstrip)
		{
			TClonesArray &cluster = *ClusterA;
			cluster[*NClusters] = new(cluster[*NClusters]) TAlphaMCCluster( cstrip->GetModule(),
									cstrip->GetASIC(),
									fStrips->GetEntries(),
									fEnergyDeposit);
			++(*NClusters);
		}
	}
	return *NClusters;
}

//_____________________________________________________________________________
Int_t TAlphaMCDigi::CreateStripOutput(TClonesArray *StripA, Int_t *NStrips, Int_t *NRawHits)
{
	Bool_t pside=kFALSE;
	Bool_t nside=kFALSE;
	if( gMC->IsTrackExiting() && ((fEnergyDeposit * 1.e6) > fEnergyThreshold) )
	{
		// Add Strips to Output Tree
		TClonesArray &strip = *StripA;
		for(Int_t i=0; i<fStrips->GetEntries(); i++)
		{
			TAlphaMCStrip *s = (TAlphaMCStrip*) fStrips->At(i);
			strip[*NStrips] = new(strip[*NStrips]) TAlphaMCStrip( s->GetModule(),
								s->GetASIC(),
								s->GetStrip(),
								s->GetADC(),
								s->GetFrac());
			(*NStrips)++;
			*NRawHits += *NStrips;

			if(s->GetASIC() == 1 || s->GetASIC() == 2) nside=kTRUE;
			if(s->GetASIC() == 3 || s->GetASIC() == 4) pside=kTRUE;
		}
	}
	
	if(pside&&nside) return 1;
	else return 0;
}

//_____________________________________________________________________________
void TAlphaMCDigi::FillTrackOutput(TAlphaMCTrack *MCTrack, Double_t EnPoint[], Int_t sides, std::vector<Int_t> *hybList)
{
	if( gMC->IsTrackExiting() && ((fEnergyDeposit * 1.e6) > fEnergyThreshold) )
	{
		//TClonesArray &track = MCTrack;
		if( fHybrid->GetLayer() == 0 ) MCTrack->SetNlayer1( MCTrack->GetNlayer1() + 1 );
		if( fHybrid->GetLayer() == 1 ) MCTrack->SetNlayer2( MCTrack->GetNlayer2() + 1 );
		if( fHybrid->GetLayer() == 2 ) MCTrack->SetNlayer3( MCTrack->GetNlayer3() + 1 );
		
		if(sides)
		{
			MCTrack->SetIsTrack( kTRUE );
  
			// check to see if the track has already left a hit in this module
			Int_t snum = fHybrid->GetLayer();
			Bool_t skip = kFALSE;
			for(Int_t is = 0; is<(Int_t)hybList->size(); is++)
			{
				Int_t snm = hybList->at(is);
				if( snm == snum )
				{
					skip = kTRUE;
					break;
				}
			}
			if(!skip)
			{
				hybList->push_back(snum);
				Int_t NSil = MCTrack->GetNSil() + 1;
				// add add hits to tree
				if( NSil < 8 )
				{     
					MCTrack->SetNSil( NSil );
					MCTrack->SetX(NSil-1,EnPoint[0]);
					MCTrack->SetY(NSil-1,EnPoint[1]);
					MCTrack->SetZ(NSil-1,EnPoint[2]);
					MCTrack->SetSilNum(NSil-1,fHybrid->GetSilNum());
				}
			}
		}
	}
	
}
