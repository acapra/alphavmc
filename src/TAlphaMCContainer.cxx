#include "TAlphaMCContainer.h"

TAlphaMCContainer::TAlphaMCContainer()
{
  // ctor
  fSilArray.SetOwner( kTRUE );
  fSilArray.Clear();
}

TAlphaMCContainer::~TAlphaMCContainer()
{
  fSilArray.Delete();
}

void TAlphaMCContainer::Clear(Option_t * /* opt */)
{
  fSilArray.Clear();
}
//end
