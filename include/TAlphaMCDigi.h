#ifndef __TAlphaMCDigi__
#define __TAlphaMCDigi__

//===========================================
// Digitization Class for ALPHA MC
// AndreaC  --- October 2012
//===========================================

#include <vector>

#include <TClonesArray.h>
#include <TGeoManager.h>

#include "TAlphaEvent.h"
#include "TAlphaEventSil.h"
#include "TAlphaMCTrack.h"

class TAlphaMCDigi
{
	private:
	TAlphaEventSil* fHybrid;
	TObjArray*      fStrips;
	Double_t        fEnergyDeposit;
	Double_t        fEnergyThreshold;
	
	public:
	TAlphaMCDigi(TGeoVolume *CurrentVol,TAlphaEvent *Event,Double_t EnergyDeposit);
	virtual ~TAlphaMCDigi();
	
	void SharedHit(Double_t EnPoint[],Double_t ExPoint[]);
	
	Int_t CreateClusterOutput(TClonesArray *ClusterA, Int_t *NClusters);
	Int_t CreateStripOutput(TClonesArray *StripA, Int_t *NStrips, Int_t *NRawHits);
	
	void FillTrackOutput(TAlphaMCTrack *Track, Double_t EnPoint[], Int_t sides, std::vector<Int_t> *hybList);
	
	TAlphaEventSil* GetHybrid() {return fHybrid;}
	TObjArray*      GetStrips() {return fStrips;}
	Double_t        GetETh()    {return fEnergyThreshold;}
	
   ClassDef(TAlphaMCDigi,1);
};
#endif
