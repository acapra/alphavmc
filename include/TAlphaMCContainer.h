// TAlphaMCContainer.h
#ifndef __TAlphaMCContainer__
#define __TAlphaMCContainer__

// Output of the Monte Carlo detector response program
// To be processes an vertex extracted later at the macro level

#include <TObject.h>
#include <TObjArray.h>

#include "TAlphaEventSilArray.h"

class TAlphaMCContainer : public TObject
{
 private:
  TObjArray fSilArray;

 public:
  TAlphaMCContainer();
  ~TAlphaMCContainer();
    
  Int_t GetN() { return fSilArray.GetEntries(); }
  
  void AddLast( TAlphaEventSilArray * array ) { fSilArray.AddLast( array ); }
  TAlphaEventSilArray * GetArray( Int_t i )   { return (TAlphaEventSilArray*) fSilArray.At( i ); }

  virtual void Clear(Option_t * opt="");

  ClassDef( TAlphaMCContainer, 1 );
};

#endif //TAlphaMCContainer.h
