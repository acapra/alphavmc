//TSiliconLibVersion.h: I am a file that should not be committed to the SVN!
//                      I am created by the makefile
#ifndef _VMCVersion_
#define _VMCVersion_
#include "TString.h"
#include <TObject.h>
class TvmcVersion: public TObject {
private:

TString _vmcSVNRevision_="Unversioned directory";
TString _vmcSVNFull_="";
unsigned int gvmcTime=1699323718;
 
public:
  TvmcVersion();
  virtual ~TvmcVersion();
  
  TString GetvmcSVNRevision() { return _vmcSVNRevision_; }
  TString GetvmcSVNFull() { return _vmcSVNFull_; }
  unsigned int GetgvmcTime() { return gvmcTime; }
  ClassDef(TvmcVersion,1);
};
#endif

