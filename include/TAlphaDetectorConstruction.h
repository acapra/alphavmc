// $Id: TAlphaDetectorConstruction.h 341 2008-05-26 11:04:57Z ivana $

//------------------------------------------------
// The Virtual Monte Carlo examples
// Copyright (C) 2007, 2008 Ivana Hrivnacova
// All rights reserved.
//
// For the licensing terms see geant4_vmc/LICENSE.
// Contact: vmc@pcroot.cern.ch
//-------------------------------------------------

/// \file TAlphaDetectorConstruction.h
/// \brief Definition of the TAlphaDetectorConstruction class 
///
/// Geant4 ExampleN06 adapted to Virtual Monte Carlo \n
/// Id: ExN06DetectorConstruction.hh,v 1.4 2003/01/23 15:34:23 maire Exp 
/// GEANT4 tag Name: geant4-07-00-cand-01 
///
/// \author I. Hrivnacova; IPN, Orsay

#ifndef TALPHA06_DETECTOR_CONSTRUCTION_H
#define TALPHA06_DETECTOR_CONSTRUCTION_H

#include <map>

#include <Riostream.h>
#include <TObject.h>
#include <TString.h>
#include "SiMod.h"

class TAlphaDetectorConstruction : public TObject
{
  public:  
    TAlphaDetectorConstruction();
    virtual ~TAlphaDetectorConstruction();

  public:
     void ConstructMaterials();
     void ConstructGeometry();
     void ConstructOpGeometry();
     
  private:      
     // data members  
     
     ClassDef(TAlphaDetectorConstruction,1); //TAlphaDetectorConstruction
};

#endif //TALPHA06_DETECTOR_CONSTRUCTION_H
