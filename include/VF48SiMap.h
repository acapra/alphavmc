#ifndef _MAP_H_
#define _MAP_H_

#include "SiMod.h"
// Declaration of map class
// Description: Map outputs from VF48 to hits on the Si detectors
// Author: Art Olin 

#include <string>

using std::string;

class VF48SiMap {

private:
  int maxmodule_;
  const int maxVF48samples_;

  //  double fadc_[int Module][int Channel][int Sample][int VA];

  int module_[nSil][4];
  int channel_[nSil][4];
  int frcnumber_[nSil][4];
  int frcport_[nSil][4];
  int ttcchannel_[nSil][4];
  int sinumber_[nVF48][48];
  string siname_[nVF48][48];
  int va_[nVF48][48];
  int frcn_[nVF48][48];
  int frcp_[nVF48][48];
  int ttcc_[nVF48][48];
  string Siname_[nSil];

public:
  VF48SiMap( const string & ); //Constructor from map file
  //VF48SiMap( int runnumber ); Constructor from database to be written
  
  ~VF48SiMap() {}  // virtual only needed if you derive from this class
  
  // New MidasEvent fn?  double* GetADC(int Module, int Channel, int Sample, int VA); 
  
  int GetVF48( const int ,const int, int &, int &, int & );
  int GetSil ( const int , const int, int &, int &, int &, int &, int &);
  string GetSilName( const int, const int);
  string GetSilName( const int );

};
#endif // _MAP_H_
