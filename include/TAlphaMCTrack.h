#ifndef __TAlphaMCTrack__
#define __TAlphaMCTrack__

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TAlphaMCTrack                                                        //
//                                                                      //
// Monte Carlo Track                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <TObject.h>
#include <TObjArray.h>
#include "TVector3.h"

#include "TAlphaMCStrip.h"

class TAlphaMCTrack : public TObject {
private:
  Int_t    fParticleID;
  Double_t fPx;
  Double_t fPy;
  Double_t fPz;
  Double_t fEtot;
  Int_t fNlayer1;
  Int_t fNlayer2;
  Int_t fNlayer3;
  Int_t    fNSil;
  Int_t    fParent;
  Int_t    fSecondParent;
  Int_t    fStatus;
  
//  TObjArray fPosition;

  Double_t fx[8];
  Double_t fy[8];
  Double_t fz[8];
  Int_t    fSilNum[8];

  Bool_t fIsTrack;

public:
  TAlphaMCTrack();
  TAlphaMCTrack( TAlphaMCTrack* & );
  virtual ~TAlphaMCTrack();
  
  void     ClearTrack();
  Double_t Etot() { return fEtot; }
  Int_t    GetParticleID() { return fParticleID; }
  Int_t    GetNlayer1() { return fNlayer1; }
  Int_t    GetNlayer2() { return fNlayer2; }
  Int_t    GetNlayer3() { return fNlayer3; }
  Int_t    GetNSil() { return fNSil; }
  Int_t    GetParent() { return fParent; }
  Int_t    GetSecondParent() { return fSecondParent; }
  Int_t    GetStatus() { return fStatus; }

  Double_t Px() { return fPx; }
  Double_t Py() { return fPy; }
  Double_t Pz() { return fPz; }

  void     SetParticleID( Int_t ID ) { fParticleID = ID; }
  void     SetPx( Double_t Px ) { fPx = Px; }
  void     SetPy( Double_t Py ) { fPy = Py; }
  void     SetPz( Double_t Pz ) { fPz = Pz; }
  void     SetNlayer1( Int_t n ) { fNlayer1 = n; }
  void     SetNlayer2( Int_t n ) { fNlayer2 = n; }
  void     SetNlayer3( Int_t n ) { fNlayer3 = n; }
  void     SetEtot( Double_t Etot ) { fEtot = Etot; }
  void     SetNSil( Int_t NSil ) { fNSil = NSil; }
  void     SetParent( Int_t parent ) { fParent = parent; }
  void     SetSecondParent( Int_t parent ) { fSecondParent = parent; }
  void     SetStatus( Int_t status ) {fStatus = status; }

  Double_t X(Int_t i) { return fx[i]; }
  Double_t Y(Int_t i) { return fy[i]; }
  Double_t Z(Int_t i) { return fz[i]; }

  void     SetX(Int_t i, Double_t x) { fx[i] = x; }
  void     SetY(Int_t i, Double_t y) { fy[i] = y; }
  void     SetZ(Int_t i, Double_t z) { fz[i] = z; }
  void     SetSilNum(Int_t i, Int_t num) { fSilNum[i] = num; }
  Int_t    GetSilNum(Int_t i) { return fSilNum[i]; }

  Bool_t   IsTrack() { return fIsTrack; }
  void     SetIsTrack( Bool_t IsTrack ) { fIsTrack = IsTrack; }
  
//   TObjArray GetPositionArray() {return fPosition;}
//   Int_t StorePosition(Double_t xx, Double_t yy, Double_t zz);
//   TVector3* RetrievePosition(Int_t i);

  ClassDef(TAlphaMCTrack,1);
};

#endif
