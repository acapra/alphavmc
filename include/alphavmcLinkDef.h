// $Id: example06LinkDef.h 341 2008-05-26 11:04:57Z ivana $

//------------------------------------------------
// The Virtual Monte Carlo examples
// Copyright (C) 2007, 2008 Ivana Hrivnacova
// All rights reserved.
//
// For the licensing terms see geant4_vmc/LICENSE.
// Contact: vmc@pcroot.cern.ch
//-------------------------------------------------

/// \file  example06LinkDef.h
/// \brief The CINT link definitions for example E06 classes

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ class  TvmcVersion+;
#pragma link C++ class  TAlphaMCApplication+;
#pragma link C++ class  TAlphaMCStack+;
#pragma link C++ class  TAlphaMCContainer;
#pragma link C++ class  TAlphaMCTrack;
#pragma link C++ class  TAlphaMCStrip;
#pragma link C++ class  TAlphaMCCluster;
#pragma link C++ class  TAlphaMCPMT;
#pragma link C++ class  TAlphaMCDigi;
#pragma link C++ class  TAlphaDetectorConstruction+;
#pragma link C++ class  TAlphaPrimaryGenerator+;
#pragma link C++ class  TAlphaEvent+;
#pragma link C++ class  TAlphaEventObject;
#pragma link C++ class  TAlphaEventHit;
#pragma link C++ class  TAlphaEventSil;
#pragma link C++ class  TAlphaEventVerbose+;
#pragma link C++ class  TAlphaDisplay+;
#pragma link C++ class  TAlphaEventNCluster;
#pragma link C++ class  TAlphaEventPCluster;
#pragma link C++ class  TAlphaEventTrack;
#pragma link C++ class  TAlphaEventStrip;
#pragma link C++ class  TAlphaEventPStrip;
#pragma link C++ class  TAlphaEventNStrip;
#pragma link C++ class  TAlphaEventVertex;
#pragma link C++ class  TAlphaEventHelix;
#pragma link C++ class  TAlphaEventCosmicHelix;
#pragma link C++ class  TAlphaGeoDetectorXML;
#pragma link C++ class  TAlphaGeoMaterialXML;
#pragma link C++ class  TAlphaGeoEnvironmentXML;
#pragma link C++ class  TAlphaGeoPMTXML;
#pragma link C++ class  TAlphaEventSilArray;
#pragma link C++ class  THoughPeakFinder;
#pragma link C++ class  THoughPeak;
#pragma link C++ class  TProjCluster+;
#pragma link C++ class  TProjClusterAna+;
#pragma link C++ class  TProjClusterBase+;
#endif
