#ifndef __TAlphaMCPMT__
#define __TAlphaMCPMT__

#include "TNamed.h"
#include "TObject.h"

class TAlphaMCPMT : public TNamed
{
private:
  Double_t fedep;
  Double_t ftime;
  
public:
  TAlphaMCPMT();
  TAlphaMCPMT( Double_t e, Double_t time, const char *name );
  ~TAlphaMCPMT();
  
  Double_t GetE() { return fedep; }
  Double_t GetTime() { return ftime; }

  ClassDef(TAlphaMCPMT,1);
};

#endif
