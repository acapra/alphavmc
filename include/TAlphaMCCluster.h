#ifndef __TAlphaMCCluster__
#define __TAlphaMCCluster__

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TAlphaMCCluster                                                        //
//                                                                      //
// Monte Carlo Cluster                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <TObject.h>

class TAlphaMCCluster : public TObject {
private:
  Int_t fModule;
  Int_t fASIC;
  Int_t fNStrips;
  Double_t fEdep;

public:
  TAlphaMCCluster();
  TAlphaMCCluster( Int_t Module, Int_t ASIC, Int_t NStrips, Double_t Edep );
  TAlphaMCCluster( TAlphaMCCluster* & );
  virtual ~TAlphaMCCluster();

  Int_t GetModule() { return fModule; }
  Int_t GetASIC() { return fASIC; }
  Int_t GetNStrips() { return fNStrips; }
  Double_t GetEdep() { return fEdep; }
  
  ClassDef(TAlphaMCCluster,1);
};

#endif
