{
  // // Load basic libraries
  gSystem->Load("libXMLParser");
  gSystem->Load("libGeom");
  gSystem->Load("libEG");
  
  TString libname("../bin/lib/libVMCLibrary.so");
  libname = gSystem->FindDynamicLibrary(libname);
  cout << "Loading: " << libname;
  Int_t s = gSystem->Load( libname );
  if(s==0) cout<<"... ok"<<endl;
  
  // Load Geant3 libraries
  libname = "../bin/lib/libgeant321.so";
  libname = gSystem->FindDynamicLibrary(libname);
  cout << "Loading: " << libname;
  s = gSystem->Load( libname );
  if(s==0) cout<<"... ok"<<endl;

  
  // Load ALPHAVMC library
  libname = "../bin/libalphavmc.so";
  libname = gSystem->FindDynamicLibrary(libname);
  cout << "Loading: " << libname;
  s = gSystem->Load( libname );
  if(s==0) cout<<"... ok"<<endl;


  TString incpath("-I../include");
  gSystem->AddIncludePath(incpath.Data());

  gStyle->SetPalette(1);
  gErrorIgnoreLevel = 5000;
}
