# To be sure your cmake version in recent enough
cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)

# Project name
project(alphavmc)

set(library_name ${PROJECT_NAME})
set(CMAKE_INSTALL_LIBDIR ${CMAKE_INSTALL_PREFIX}/lib)

#----------------------------------------------------------------------------
# Include the required packages
#
# https://root.cern/manual/integrate_root_into_my_cmake_project/
# https://cliutils.gitlab.io/modern-cmake/chapters/packages/ROOT.html
find_package(ROOT 6.16 CONFIG REQUIRED)
include(${ROOT_USE_FILE})

find_package(Geant3 CONFIG REQUIRED)
include_directories(${Geant3_INCLUDE_DIRS})
message(STATUS "Geant3 libs: " ${Geant3_LIBRARIES})
find_package(VMC CONFIG REQUIRED)
include_directories(${VMC_INCLUDE_DIRS})
message(STATUS "VMC libs: " ${VMC_LIBRARIES})

#----------------------------------------------------------------------------
# Generate Root dictionaries
#
file(GLOB tobjs ${PROJECT_SOURCE_DIR}/include/T*.h)
ROOT_GENERATE_DICTIONARY(
  G__${library_name}
  ${tobjs}
  MODULE ${library_name}
  LINKDEF include/${library_name}LinkDef.h)

#----------------------------------------------------------------------------
# Locate sources and headers for this project
#
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cxx)

add_library(${library_name} SHARED ${sources} G__${library_name})
target_include_directories(${library_name} PRIVATE ${PROJECT_SOURCE_DIR}/include ${Geant3_INCLUDE_DIRS})
target_link_libraries(${library_name} ${ROOT_LIBRARIES} ${VMC_LIBRARIES} ${Geant3_LIBRARIES})

install(TARGETS ${library_name} DESTINATION ${CMAKE_INSTALL_LIBDIR})
install(FILES ${PROJECT_BINARY_DIR}/lib${library_name}_rdict.pcm DESTINATION ${CMAKE_INSTALL_LIBDIR})
install(FILES ${PROJECT_BINARY_DIR}/lib${library_name}.rootmap DESTINATION ${CMAKE_INSTALL_LIBDIR})
